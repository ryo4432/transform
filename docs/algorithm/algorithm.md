アルゴリズム {#algorithm}
===

## 度<->ラジアン

### deg2rad

度からラジアンへの変換


### rad2deg

ラジアンから度への変換

## 正規化

### normalize_deg

度を正規化する

### normalize_rad

ラジアンを正規化する

## 表

