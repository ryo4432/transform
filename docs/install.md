# doxygen環境のインストール {#doxygen-install}

以下を叩けばOK

```bash
$ sudo apt update
$ sudo apt install doxygen doxygen-gui graphviz
$ sudo apt install texlive-lang-cjk xdvik-ja evince
$ sudo apt install texlive-fonts-recommended texlive-fonts-extra
```
