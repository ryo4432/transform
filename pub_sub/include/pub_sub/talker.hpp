/*
# Copyright 2020 Open Source Robotics Foundation, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
*/

#ifndef PUB_SUB__TALKER_HPP_
#define PUB_SUB__TALKER_HPP_

#include <chrono>
#include <cstdio>
#include <memory>
#include <string>
#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"

namespace pub_sub
{
class Talker : public rclcpp::Node
{
private:
  rclcpp::Publisher<std_msgs::msg::String>::SharedPtr pub_;
  rclcpp::TimerBase::SharedPtr timer_;
  int count_;

public:
  Talker(const std::string & node_name, const std::string & topic_name)
  : Node(node_name), count_(0)
  {
    pub_ = this->create_publisher<std_msgs::msg::String>(topic_name, 10);

    auto msg_ = std_msgs::msg::String();
    msg_.data = "Hello world";

    auto publish_message =
      [this, msg_]() -> void {
        auto message = std_msgs::msg::String();
        message.data = "Hello, world!" + std::to_string(count_++);
        RCLCPP_INFO(this->get_logger(), "Publisher: '%s'", message.data.c_str());
        std::flush(std::cout);
        pub_->publish(message);
      };

    timer_ = create_wall_timer(std::chrono::milliseconds(100), publish_message);
  }
};
}  // namespace pub_sub

#endif  // PUB_SUB__TALKER_HPP_
