/*
# Copyright 2020 Ryo Yoshimitsu
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
*/

#ifndef TRANSFORM__TRANSFORM_HPP_
#define TRANSFORM__TRANSFORM_HPP_

#include <math.h>
#include "transform/visibility.h"

namespace transform
{
TRANSFORM_PUBLIC double deg2rad(double deg);
TRANSFORM_PUBLIC double rad2deg(double rad);
TRANSFORM_PUBLIC double normalize_rad(double rad);
TRANSFORM_PUBLIC double normalize_deg(double deg);

}  // namespace transform

#endif  // TRANSFORM__TRANSFORM_HPP_
