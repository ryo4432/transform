/*
# Copyright 2020 Ryo Yoshimitsu
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
*/


#ifndef TRANSFORM__VISIBILITY_H_
#define TRANSFORM__VISIBILITY_H_

#ifdef __cplusplus
extern "C"
{
#endif

// This logic was borrowed (then namespaced) from the examples on the gcc wiki:
//     https://gcc.gnu.org/wiki/Visibility

#if defined _WIN32 || defined __CYGWIN__

  #ifdef __GNUC__
    #define TRANSFORM_EXPORT __attribute__ ((dllexport))
    #define TRANSFORM_IMPORT __attribute__ ((dllimport))
  #else
    #define TRANSFORM_EXPORT __declspec(dllexport)
    #define TRANSFORM_IMPORT __declspec(dllimport)
  #endif

  #ifdef TRANSFORM_DLL
    #define TRANSFORM_PUBLIC TRANSFORM_EXPORT
  #else
    #define TRANSFORM_PUBLIC TRANSFORM_IMPORT
  #endif

  #define TRANSFORM_PUBLIC_TYPE TRANSFORM_PUBLIC

  #define TRANSFORM_LOCAL

#else

  #define TRANSFORM_EXPORT __attribute__ ((visibility("default")))
  #define TRANSFORM_IMPORT

  #if __GNUC__ >= 4
    #define TRANSFORM_PUBLIC __attribute__ ((visibility("default")))
    #define TRANSFORM_LOCAL  __attribute__ ((visibility("hidden")))
  #else
    #define TRANSFORM_PUBLIC
    #define TRANSFORM_LOCAL
  #endif

  #define TRANSFORM_PUBLIC_TYPE
#endif

#ifdef __cplusplus
}
#endif

#endif  // TRANSFORM__VISIBILITY_H_
