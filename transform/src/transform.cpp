/*
# Copyright 2020 Ryo Yoshimitsu
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
*/

#include "transform/transform.hpp"

namespace transform
{

/**
 * @fn deg2rad
 * 度からラジアンに変換する
 * @param (deg) 度
 * @return ラジアン
 */
double deg2rad(double deg)
{
  return deg / 180.0 * M_PI;
}

/**
 * @fn rad2deg
 * ラジアンから度に変換する
 * @param (rad) ラジアン
 * @return 度
 */
double rad2deg(double rad)
{
  return rad / M_PI * 180.0;
}

/**
 * @fn normalize_rad
 * ラジアンを正規化する(-pi~piにする)
 * @param (rad) ラジアン
 * @return 正規化済みのラジアン
 */
double normalize_rad(double rad)
{
  return atan2(sin(rad), cos(rad));
}

/**
 * @fn normalize_deg
 * ラジアンを正規化する(-180~180にする)
 * @param (deg) 度
 * @return 正規化済みの度
 */
double normalize_deg(double deg)
{
  double rad_normalized = normalize_rad(deg2rad(deg));
  return rad2deg(rad_normalized);
}

}  // namespace transform
