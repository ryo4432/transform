/*
# Copyright 2020 Ryo Yoshimitsu
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
*/

#include "gtest/gtest.h"
#include "transform/transform.hpp"

TEST(transform, deg2rad)
{
  // 30 deg <-> pi/6 rad
  double expected = 30.0 / 180.0 * M_PI;
  double actual = transform::deg2rad(30.0);
  EXPECT_EQ(expected, actual);

  // -30 deg <-> -pi/6 rad
  expected = -30.0 / 180.0 * M_PI;
  actual = transform::deg2rad(-30.0);
  EXPECT_EQ(expected, actual);

  // 0 deg <-> 0 rad
  expected = 0.0 / 180.0 * M_PI;
  actual = transform::deg2rad(0.0);
  EXPECT_EQ(expected, actual);

  // 180 deg <-> pi rad
  expected = 180.0 / 180.0 * M_PI;
  actual = transform::deg2rad(180.0);
  EXPECT_EQ(expected, actual);

  // -180 deg <-> -pi rad
  expected = -180.0 / 180.0 * M_PI;
  actual = transform::deg2rad(-180.0);
  EXPECT_EQ(expected, actual);
}

TEST(transform, rad2deg)
{
  // pi/4 rad <-> 45 deg
  double expected = M_PI / 4 * 180.0 / M_PI;
  double actual = transform::rad2deg(M_PI / 4);
  EXPECT_EQ(expected, actual);

  // -pi/4 rad <-> -45 deg
  expected = -M_PI / 4 * 180.0 / M_PI;
  actual = transform::rad2deg(-M_PI / 4);
  EXPECT_EQ(expected, actual);

  // 0 rad <-> 0 deg
  expected = 0.0 * 180.0 / M_PI;
  actual = transform::rad2deg(0.0);
  EXPECT_EQ(expected, actual);

  // pi rad <-> 180 deg
  expected = M_PI * 180.0 / M_PI;
  actual = transform::rad2deg(M_PI);
  EXPECT_EQ(expected, actual);

  // -pi rad <-> 180 deg
  expected = -M_PI * 180.0 / M_PI;
  actual = transform::rad2deg(-M_PI);
  EXPECT_EQ(expected, actual);
}

TEST(transform, normalize_rad)
{
  // 9/4pi rad <-> pi/4 rad
  double expected = M_PI / 4;
  double actual = transform::normalize_rad(M_PI / 4 + 2 * M_PI);
  EXPECT_NEAR(expected, actual, 10e-6);

  // -9/4pi rad <-> -pi/4 rad
  expected = -M_PI / 4;
  actual = transform::normalize_rad(-M_PI / 4 - 2 * M_PI);
  EXPECT_NEAR(expected, actual, 10e-6);

  // 0 rad <-> 0 rad
  expected = 0;
  actual = transform::normalize_rad(0);
  EXPECT_NEAR(expected, actual, 10e-6);

  // 3pi rad <-> pi rad
  expected = M_PI;
  actual = transform::normalize_rad(M_PI + 2 * M_PI);
  EXPECT_NEAR(expected, actual, 10e-6);

  // -3pi rad <-> pi rad
  expected = -M_PI;
  actual = transform::normalize_rad(-M_PI - 2 * M_PI);
  EXPECT_NEAR(expected, actual, 10e-6);
}

TEST(transform, normalize_deg)
{
  // 405.0 deg <-> 45.0 deg
  double expected = 45.0;
  double actual = transform::normalize_deg(360.0 + 45.0);
  EXPECT_NEAR(expected, actual, 10e-6);

  // -405.0 deg <-> -45.0 deg
  expected = -45.0;
  actual = transform::normalize_deg(-360.0 - 45.0);
  EXPECT_NEAR(expected, actual, 10e-6);

  // 0 rad <-> 0 rad
  expected = 0;
  actual = transform::normalize_rad(0);
  EXPECT_NEAR(expected, actual, 10e-6);

  // 540.0 deg <-> 180.0 deg
  expected = 180.0;
  actual = transform::normalize_deg(360.0 + 180.0);
  EXPECT_NEAR(expected, actual, 10e-6);

  // -540.0 deg <-> -180.0 deg
  expected = -180.0;
  actual = transform::normalize_deg(-360.0 - 180.0);
  EXPECT_NEAR(expected, actual, 10e-6);
}
